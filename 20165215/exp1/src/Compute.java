public class Compute {
    public static void main (String args[]) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = gcd (a,b);
        int d = lcm (a,b);
        System.out.println(a+"和"+b+"的最大公约数是:"+c+",最小公倍数是:"+d);
    }       
    public static int gcd (int a,int b) { //求a和b的最大公约数
        if(a==0) return 1;
        if(a<b) {
           int c=a;
           a=b;
           b=c;
        }
        int r=a%b;
        while(r!=0) {
           a=b;
           b=r;
           r=a%b;
        }
        return b;
    }
    public static int lcm (int a,int b) {  //求a和b的最小公倍数
        int c=(a*b)/gcd(a,b);   
        return c;
    }
}
