package exp2;

import static org.junit.Assert.*;
import org.junit.Test;
/**
 * Created by 匪夷所思 on 2018/4/16.
 */
public class StringBufferDemoTest {
    StringBuffer a = new StringBuffer("StringBuffer");//测试12个字符
    StringBuffer b = new StringBuffer("StringBuffer12345");//测试17个字符
    @Test
    public void testcharAt(){
        assertEquals('S',a.charAt(0));
        assertEquals('r',a.charAt(11));
    }
    @Test
    public void testlength(){
        assertEquals(12,a.length());
        assertEquals(17,b.length());
    }
    @Test
    public void testcapacity(){
        assertEquals(28,a.capacity());
        assertEquals(33,b .capacity());
    }
    @Test
    public void testindexof(){
        assertEquals(1,a.indexOf("tring"));
        assertEquals(12,b .indexOf("12345"));
    }
    @Test
    public void testtoString(){
        assertEquals("StringBuffer",a.toString());
        assertEquals("StringBuffer12345",b .toString());
    }
}