import java.lang.String;
import java.io.IOException;
import java.util.Scanner;
import java.util.Random;
public class compute4 {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.print("输入四则运算题目的数量: ");
        int number= scan.nextInt();
        double result=0.0,answer ;
        double right=0,rate;
        for(int i=0;i<number;i++){
            Random rand=new Random();
            int shu1=rand.nextInt(100);
            int shu2=rand.nextInt(100);
            System.out.print("题目"+(i+1)+":");
            switch(rand.nextInt(3)){
                case 0:
                    result=caculate.addtion(shu1,shu2);
                    System.out.println(shu1+" + "+shu2);
                    break;
                case 1:
                    result=caculate.subtraction(shu1,shu2);
                    System.out.println(shu1+" - "+shu2);
                    break;
                case 2:
                    result=caculate.multiplication(shu1,shu2);
                    System.out.println(shu1+" * "+shu2);
                    break;
                case 3:
                    result=caculate.division(shu1,shu2);
                    System.out.println(shu1+" / "+shu2);
                    break;
            }
            System.out.println("你的答案:");
            answer = scan.nextInt();
            if (answer == result) {
                System.out.println("正解！");
                right++;
            } else
                System.out.println("错解！正确答案是" + result);
        }
        rate = (double) right / number;
        System.out.println("检测结束，你的正确率是：" + rate);
    }

}
