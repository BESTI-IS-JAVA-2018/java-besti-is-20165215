package exp3;

import java.io.*;
import java.security.*;


//非对称加密-RSA算法1
//创建RSA公钥和私钥
public class Skey_RSA {
    public static void main(String args[]) throws Exception {
        // 创建密钥对生成器
        KeyPairGenerator kpg = KeyPairGenerator.getInstance ( "RSA" );
        //初始化密钥长度
        kpg.initialize ( 1024 );
        //生成密钥对
        KeyPair kp = kpg.genKeyPair ();
        //获取公钥和私钥
        PublicKey pbkey = kp.getPublic ();
        PrivateKey prkey = kp.getPrivate ();
        // 使用对象流将密钥保存在文件中，加密所用的公钥和解密所用的私钥分开保存。
        FileOutputStream f1 = new FileOutputStream ( "Skey_RSA_pub.dat" );
        ObjectOutputStream b1 = new ObjectOutputStream ( f1 );
        b1.writeObject ( pbkey );
        FileOutputStream f2 = new FileOutputStream ( "Skey_RSA_priv.dat" );
        ObjectOutputStream b2 = new ObjectOutputStream ( f2 );
        b2.writeObject ( prkey );
    }
}
