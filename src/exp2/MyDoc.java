package exp2;

/**
 * Created by 匪夷所思 on 2018/4/16.
 */
abstract class Data {
    abstract public void DisplayValue();
}

class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}

class Long extends  Data {
    long value;
    Long() {
        value=20165215;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}

class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class LongFactory extends Factory {
    public Data CreateDataObject(){
        return new Long();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new LongFactory());
        d.DisplayData();
    }
}