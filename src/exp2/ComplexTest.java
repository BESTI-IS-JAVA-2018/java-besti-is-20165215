/**
 * Created by 陈思兵 on 2018/4/26.
 */
import org.testng.annotations.Test;

import static junit.framework.Assert.assertEquals;


public class ComplexTest {
    Complex a = new Complex(0, 0);
    Complex b = new Complex(1, -1);
    Complex c = new Complex(-2,2);
    @Test
    public void testComplexAdd() throws Exception {
        assertEquals("1.0-1.0*i", a.ComplexAdd(b).toString());
        assertEquals("-2.0+2.0*i", a.ComplexAdd(c).toString());
        assertEquals("-1.0+1.0*i", b.ComplexAdd(c).toString());
    }
    @Test
    public void testComplexSub() throws Exception {
        assertEquals("-1.0+1.0*i", a.ComplexSub(b).toString());
        assertEquals("2.0-2.0*i", a.ComplexSub(c).toString());
        assertEquals("3.0-3.0*i", b.ComplexSub(c).toString());
    }
    @Test
    public void testComplexMulti() throws Exception {
        assertEquals("0.0", a.ComplexMulti(b).toString());
        assertEquals("-0.0", a.ComplexMulti(c).toString());
        assertEquals("-2.0-2.0*i", b.ComplexMulti(c).toString());
    }
    @Test
    public void testComplexComplexDiv() throws Exception {
        assertEquals("0.0", a.ComplexDiv(b).toString());
        assertEquals("0.0", a.ComplexDiv(c).toString());
        assertEquals("0.5-0.5*i", b.ComplexDiv(c).toString());
    }
}
