package exp2; /**
 * Created by 匪夷所思 on 2018/4/16.
 */
import java.util.*;
public class Complex {
    private double RealPart;//复数的实部
    private double ImagePart;//复数的虚部
    public Complex(){
        this.RealPart= 0;
        this.ImagePart= 0;
    }
    public Complex(double R,double I){
        this.RealPart= R;
        this.ImagePart= I;
    }
    public String toString(){
        if(ImagePart>0.0){
            if(RealPart==0.0)
                return ImagePart+"*i";
            else
                return RealPart+"+"+ImagePart+"*i";
        }
        else if(ImagePart<0.0){
            if(RealPart==0.0)
                return ImagePart+"*i";
            else
                return RealPart+""+ImagePart+"*i";
        }
        else
            return RealPart+"";
    }
    public Complex ComplexAdd(Complex a){
        double NewRealPart=this. RealPart+a.RealPart;
        double NewImagePart=this.ImagePart+a.ImagePart;
        return new Complex(NewRealPart,NewImagePart);
    }
    public Complex ComplexSub(Complex a){
        double NewRealPart=this. RealPart-a.RealPart;
        double NewImagePart=this.ImagePart-a.ImagePart;
        return new Complex(NewRealPart,NewImagePart);
    }
    public Complex ComplexMulti(Complex a){
        double NewRealPart=this. RealPart*a.RealPart;
        double NewImagePart=this.ImagePart*a.ImagePart;
        return new Complex(NewRealPart,NewImagePart);
    }
    public Complex ComplexDiv(Complex a){
            double NewRealPart= (RealPart * a.ImagePart+ ImagePart * a.RealPart)/(a.ImagePart * a.ImagePart + a.RealPart * a.RealPart);
            double NewImagePart=(ImagePart * a.ImagePart + RealPart * a.RealPart)/(a.ImagePart * a.ImagePart + a.RealPart * a.RealPart);
            return new Complex(NewRealPart,NewImagePart);
    }
    public boolean equals(Object obj) {
        if(this==obj){
            return true;
        }
        if(!(obj instanceof Complex)) {
            return false;
        }
        Complex complex=(Complex)obj;
        if(complex.RealPart!=((Complex)obj).RealPart){
            return false;
        }
        if(complex.ImagePart!=((Complex)obj).ImagePart){
            return false;
        }
        return true;
    }
}
