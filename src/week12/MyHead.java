package week12;
import java.io.*;
/**
 * Created by 匪夷所思 on 2018/5/17.
 */
public class MyHead {
    public static void main(String[] args){
        File fRead = new File("20165215.txt");
        int number =Integer.parseInt(args[0]);
        try {
            Reader in = new FileReader(fRead);
            BufferedReader bufferRead =new BufferedReader(in);
            String str = null;
            int i=0;
            while((str=bufferRead.readLine())!=null&&i<number) {
                System.out.println(str);
                i++;
            }
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}

