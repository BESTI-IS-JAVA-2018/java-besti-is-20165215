package week10;

import java.util.*;
class StudentKey implements Comparable {
    double d=0;
    String s="";
    StudentKey (double d) {
        this.d=d;
    }
    StudentKey (String s) {
        this.s=s;
    }
    public int compareTo(Object b) {
        StudentKey st=(StudentKey)b;
        if((this.d-st.d)==0)
            return -1;
        else
            return (int)((this.d-st.d)*1000);
    }
}
class StudentIF {
    String name=null;
    String id;
    double math,english,computer,total,aver;
    StudentIF(String s, String d,double m, double e, double f, double a,double b) {
        id=d;
        name=s;
        math=m;
        english=e;
        computer=f;
        total=a;
        aver=b;
    }
}
public class Student {
    public static void main(String args[]) {
        TreeMap<StudentKey, StudentIF> treemap = new TreeMap<StudentKey, StudentIF>();
        String str[] = {"周启航", "朱文远", "张家佳", "陈厚康", "叶  佺"};
        String id[] = {"20165213","20165214","20165215","20165216","20165217"};
        double math[] = {89, 45, 78, 76, 70};
        double english[] = {67, 68, 69, 70, 71};
        double computer[] = {76, 66, 30, 65, 80};
        double total[] = new double[5];
        double aver[] = new double[5];
        StudentIF student[] = new StudentIF[5];
        for (int k = 0; k < student.length; k++) {
            total[k] = math[k] + english[k] + computer[k];
            aver[k] = total[k] / 3;
        }
        for (int k = 0; k < student.length; k++) {
            student[k] = new StudentIF(str[k], id[k],math[k], english[k], computer[k], total[k], aver[k]);
        }
        StudentKey key[] = new StudentKey[5];
        for (int k = 0; k < key.length; k++) {
            key[k] = new StudentKey(student[k].id);
        }
        for (int k = 0; k < student.length; k++) {
            treemap.put(key[k],student[k]);
        }
        int number = treemap.size();
        System.out.println("树映射中有" + number + "个对象,按学号排序:");
        Collection<StudentIF> collection = treemap.values();
        Iterator<StudentIF> iter = collection.iterator();
        while (iter.hasNext()) {
            StudentIF stu = iter.next();
            System.out.println("姓名 " + stu.name + " 学号 " + stu.id);
        }
        treemap.clear();
        for (int k = 0; k < key.length; k++) {
            key[k] = new StudentKey(student[k].total);
        }
        for (int k = 0; k < student.length; k++) {
            treemap.put(key[k], student[k]);
        }
         number = treemap.size();
        System.out.println("树映射中有" + number + "个对象,按总成绩排序:");
         collection = treemap.values();
         iter = collection.iterator();
        while (iter.hasNext()) {
            StudentIF stu = iter.next();
            System.out.println("姓名 " + stu.name + " 总成绩 " + stu.total);
        }
    }
}