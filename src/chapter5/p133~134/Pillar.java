public class Pillar {
    Geometry bottom;	//bottom是用抽象类Geometry声明的对象
    double height;
    Pillar (Geometry bottom,double height) {
      this.bottom = bottom;
      this.height = height;
    }
    public double getVolume() {
      if (bottom==null) {
        System.out.println("没有底，无法计算体积");
        return -1;
      }
      return bottom.getArea()*height;
    }
}
