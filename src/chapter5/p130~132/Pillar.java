public class Pillar {
    Circle  bottom;	//bottom是用具体类Circle声明的对象
    double height; 
    Pillar (Circle bottom,double height) {
      this.bottom = bottom;
      this.height = height;
    }
    public double getVolume(){
      return bottom.getArea()*height;
    }
}
