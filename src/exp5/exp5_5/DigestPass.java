package exp5.exp5_5;
import java.security.*;
/**
 * Created by 匪夷所思 on 2018/5/28.
 */
public class DigestPass {
    public static String MD5(String ss) throws Exception{
        String x=ss;
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(x.getBytes("UTF8"));
        byte s[ ]=m.digest( );
        String result="";
        for (int i=0; i<s.length; i++){
            result+=Integer.toHexString((0x000000ff & s[i]) |
                    0xffffff00).substring(6);
        }
        return result;
    }
}
