package exp5.exp5_3;

import exp5.exp5_1.*;
import java.io.*;
import java.net.*;
/**
 * Created by 匪夷所思 on 2018/5/28.
 */
public class Server {
    public static void main(String[] args) {
        ServerSocket serverForClient=null;
        Socket socketOnServer= null;
        DataOutputStream out=null;
        DataInputStream  in=null;
        try {
            serverForClient = new ServerSocket(8765);
        }
        catch (IOException e) {
            System.out.println(e);
        }
        try{
            String psw = "caicaiwoshishui";
            System.out.println("等待客户呼叫");
            socketOnServer = serverForClient.accept(); //堵塞状态，除非有客户呼叫
            out=new DataOutputStream(socketOnServer.getOutputStream());
            in=new DataInputStream(socketOnServer.getInputStream());
            String s = in.readUTF(); // in读取信息，堵塞状态
            System.out.println("服务器收到客户的提问:"+s);
            String m = new String(Crypt.decrypt(Crypt.parseHexStr2Byte(s),psw));
            System.out.println("解密后得到:"+m);
            MyDC evaluator = new MyDC();
            int  result = evaluator.evaluate(m);
            out.writeUTF(result+" ");
            out.writeUTF(result +" ");
        }
        catch(Exception e) {
            System.out.println("客户已断开"+e);
        }
    }
}
