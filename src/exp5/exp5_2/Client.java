package exp5.exp5_2;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 * Created by 匪夷所思 on 2018/5/27.
 */
public class Client {
    public static void main(String[] args) {
        Socket mysocket;
        DataInputStream in=null;
        DataOutputStream out=null;

        try {
            Scanner sc = new Scanner(System.in);
            mysocket=new Socket("localhost",8765);
            System.out.println("连接成功，请输入表达式" );
            in=new DataInputStream(mysocket.getInputStream());
            out=new DataOutputStream(mysocket.getOutputStream());
            String expression=sc.nextLine();
            out.writeUTF(expression);
            String  s=in.readUTF();
            System.out.println("客户收到服务器的回答:");
            System.out.println(expression + " = " + s);
        }
        catch(Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}
